# Documentation for the 2D PBC QMMM Development Project

Link to the book: https://theochem-ui.gitlab.io/doc-2d-pbc-qmmm/

This is the Jupyter book documentation project for the 2D PBC QMMM
Development Project hosted by the Thechem-UI group intended
to be deployed to an access-controlled gitlab Pages website.

All entry pages in the book relate to issues in the GPAW and ASE
repositories in the Theochem-UI namespace.
