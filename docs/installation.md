# Download and Install

### Implementation details

The SCME code has a C++ backend that is coupled to the atomistic simulation environment (ASE) frontend via python binding (using the framework pybind11). 

### Downloading the code:
```bash
git clone https://gitlab.com/AnoopANair/scmecpp.git
```
### Installation procedure: 



**Conda:**
```bash
conda create --name SCMEENV
conda activate SCMEENV
conda install -c conda-forge gpaw
conda install -c conda-forge ase
conda install cmake
conda install -c conda-forge pybind11
conda install -c conda-forge gcc
```
