
<style>
    .main_container{
        text-align:justify;
    }
    #math {
    width: 100px;
}
</style>


# SCME Theory 

<div class="main_container">
The vast majority of interaction potentials are based in one way or another on the long- and short-range perturbation theories of intermolecular interactions. The former applies when the separation between molecules is sufficiently large for the overlap between wave functions to be insignificant. In such a case the exact expression for the interaction energy reduces to a sum of electrostatic, induction and dispersion terms. At shorter distances, however, the exchange repulsion and in some cases the charge-transfer arising from the overlap cannot be ignored.
</div>

<div class="main_container">
Following this approach, we have defined the total interaction energy between water molecules as the sum of electrostatic, induction, dispersion and short-range repulsion terms:
</div>

$$ Etot = E_{es+ind} + E_{disp} + E_{rep}.$$ 

<h3>Energy components</h3>

<div class="main_container">

<b><u>Electrostatic and induction energy</u>: </b>The electrostatic interaction between the molecules is described in terms of a single-center multipole expansion. The molecules are modelled as a collection of multipole moments located at the centers of mass. It has been demonstrated that in order to reach convergence in the multipole expansion of the electric field the expansion had to be carried out up to and including the hexadecapole moment. Dipole–dipole, dipole–quadrupole and quadrupole–quadrupole polarizabilities were included to account for the induction effects. Within this approximation, the electrostatic+induction component takes the following form:
</div>

$$ E_{es+ind} = -\frac{1}{2}\Sigma_i (\mu^i_\alpha F^i_\alpha + \frac{1}{3}\theta^i_{\alpha\beta} F^i_{\alpha\beta} + \frac{1}{15}\Omega^i_{\alpha\beta\gamma} F^i_{\alpha\beta\gamma} + \frac{1}{105}\phi^i_{\alpha\beta\gamma\delta} F^i_{\alpha\beta\gamma\delta})$$


<div class="main_container">

<b><u>Dispersion energy</u>:</b> The dispersion component of the interaction energy is:

where $r_{ij}$ is the O–O distance. Only the first three terms of the
dispersion expansion were included. The Cn coefficients used
were those recommended by Wormer and Hettema. 
At short distances, each component is switched off by means of
a Tang–Toennies damping function.


</div>


<div class="main_container">

<b><u>Repulsion energy</u>:</b> For the exchange repulsion, a modified Born–Mayer potential
was used:

The density of molecules at a given molecule was defined as a
sum over exponential weight functions, located at each one of

</div>




# Gradients of the Electrostatic Potential

Gradients of the electrostatic potential are given by the successive use of the gradient operator ($\nabla_\xi$) on

$$
\begin{align}
 V(r^a) =& \sum_{i=\alpha}^3 \frac{M^{(1)}_{l}(i,j,k)\mu_\alpha r_\alpha}{r^3} + \sum_{\alpha=1}^3\sum_{\beta=\alpha}^3\frac{M^{(2)}_{l}(i,j,k)\theta_{\alpha\beta}r_\alpha r_\beta}{r^5} +
 \sum_{\alpha=1}^3\sum_{\beta=\alpha}^3\sum_{\gamma=\beta}^3\frac{M^{(3)}_{l}(i,j,k)\Omega_{\alpha\beta\gamma}r_\alpha r_\beta r_\gamma}{r^7} + \sum_{\alpha=1}^3\sum_{\beta=\alpha}^3\sum_{\gamma=\beta}^3\sum_{\delta=\gamma}^3
 \frac{M^{(4)}_{l}(i,j,k)\Phi_{\alpha\beta\gamma\delta}r_\alpha r_\beta r_\gamma r_\delta}{r^9} \nonumber \\
 =& \frac{(\mu r)}{r^3} + \frac{(\theta rr)}{r^5} + \frac{(\Omega rrr)}{r^7} + \frac{(\Phi rrrr)}{r^9}
\end{align}
$$

We split them up into individual contributions. As such, terms which depend on the distance to high order can more readily be excluded. The symmetries for each gradient rand is also written out, which makes it relatively straightforward to implement the expression without having to make use of delta functions.

## Dipole gradients

Rank 1

\begin{equation}
  V^\mu_\alpha = -3\frac{(\mu r)}{r^5}r_\alpha + \frac{\mu_\alpha}{r^3}
\end{equation}

Rank 2

\begin{align}
 V^\mu_{\alpha\beta} =& 15\frac{(\mu r)}{r^7}r_\alpha r_\beta - 3\frac{(\mu_\alpha r_\beta + \mu_\beta r_\alpha)}{r^5} \\
 V^\mu_{\alpha\alpha} =& V^\mu_{\alpha\beta}\delta_{\alpha\beta} - 3\frac{(\mu r)}{r^5}
\end{align}

Rank 3

\begin{align}
 V^\mu_{\alpha\beta\gamma} =& -105\frac{(\mu r)}{r^9}r_\alpha r_\beta r_\gamma + 15\frac{(\mu_\alpha r_\beta r_\gamma + \mu_\beta r_\alpha r_\gamma + \mu_\gamma r_\alpha r_\beta)}{r^7} \\
 V^\mu_{\alpha\alpha\alpha} =& V^\mu_{\alpha\beta\gamma}\delta_{\alpha\beta}\delta_{\beta\gamma} + 45\frac{(\mu r)}{r^7}r_\alpha - 9\frac{\mu_\alpha}{r^5} \\
 V^\mu_{\alpha\beta\beta} =& V^\mu_{\alpha\beta\gamma}\delta_{\beta\gamma} + 15\frac{(\mu r)}{r^7}r_\alpha - 3\frac{\mu_\alpha}{r^5}
\end{align}

Rank 4

\begin{align}
 V^\mu_{\alpha\beta\gamma\delta} =& 945\frac{(\mu r)}{r^{11}}r_\alpha r_\beta r_\gamma r_\delta - 105\frac{(\mu_\alpha r_\beta r_\gamma r_\delta + \mu_\beta r_\alpha r_\gamma r_\delta + \mu_\gamma r_\alpha r_\beta r_\delta + \mu_\delta r_\alpha r_\beta r_\gamma)}{r^{9}} \\
 V^\mu_{\alpha\alpha\alpha\alpha} =& V^\mu_{\alpha\beta\gamma\delta}\delta_{\alpha\beta}\delta_{\beta\gamma}\delta_{\gamma\delta} - 630\frac{(\mu r)}{r^9}r_\alpha r_\alpha + 180\frac{\mu_\alpha r_\alpha}{r^7} + 45\frac{(\mu r)}{r^7} \\
 V^\mu_{\alpha\alpha\alpha\gamma} =& V^\mu_{\alpha\beta\gamma\delta}\delta_{\alpha\beta}\delta_{\beta\gamma} - 315\frac{(\mu r)}{r^9}r_\alpha r_\delta + 
 45 \frac{(\mu_\alpha r_\delta + \mu_\delta r_\alpha)}{r^7} \\
 V^\mu_{\alpha\alpha\delta\delta} =& V_{\alpha\beta\gamma\delta}\delta_{\alpha\beta}\delta_{\gamma\delta} -105\frac{(\mu r)}{r^9}(r_\delta r_\delta + r_\alpha r_\alpha) 
 + 30 \frac{(\mu_\alpha r_\alpha + \mu_\delta r_\delta)}{r^7} \\
 V^\mu_{\alpha\alpha\gamma\delta} =& V_{\alpha\beta\gamma\delta}\delta_{\alpha\beta} - 105\frac{(\mu r)}{r^9}r_\gamma r_\delta + 15\frac{(\mu_\gamma r_\delta + \mu_\delta r_\gamma)}{r^7}
\end{align}

Rank 5

\begin{align}
 V^\mu_{\alpha\beta\gamma\delta\eta} =& -10395\frac{(\mu r)}{r^{13}}r_\alpha r_\beta r_\gamma r_\delta r_\eta + 
 945\frac{(\mu_\alpha r_\beta r_\gamma r_\delta r_\eta + \mu_\beta r_\alpha r_\gamma r_\delta r_\eta + \mu_\gamma r_\alpha r_\beta r_\delta r_\eta
 + \mu_\delta r_\alpha r_\beta r_\gamma r_\eta + \mu_\eta r_\alpha r_\beta r_\gamma r_\delta)}{r^{11}} \\
 V^\mu_{\alpha\alpha\alpha\alpha\alpha} =& V^\mu_{\alpha\beta\gamma\delta\eta}\delta_{\alpha\beta}\delta_{\beta\gamma}\delta_{\gamma\delta}\delta_{\delta\eta} 
 + 8190\frac{(\mu r)}{r^{11}}r_\alpha r_\alpha r_\alpha - 3150\frac{\mu_\alpha}{r^{9}}r_\alpha r_\alpha - 1575\frac{(\mu r)}{r^9}r_\alpha + 225\frac{\mu_\alpha}{r^7} \\
 V^\mu_{\alpha\alpha\alpha\alpha\eta} =& V^\mu_{\alpha\beta\gamma\delta\eta}\delta_{\alpha\beta}\delta_{\beta\gamma}\delta_{\gamma\delta} + \\
 V^\mu_{\alpha\alpha\alpha\eta\eta} =& V^\mu_{\alpha\beta\gamma\delta\eta}\delta_{\alpha\beta}\delta_{\beta\gamma}\delta_{\delta\eta} + \\
 V^\mu_{\alpha\alpha\alpha\delta\eta} =& V^\mu_{\alpha\beta\gamma\delta\eta}\delta_{\alpha\beta}\delta_{\beta\gamma} + \\
 V^\mu_{\alpha\alpha\delta\delta\eta} =& V^\mu_{\alpha\beta\gamma\delta\eta}\delta_{\alpha\beta}\delta_{\gamma\delta} + \\
 V^\mu_{\alpha\alpha\gamma\delta\eta} =& V^\mu_{\alpha\beta\gamma\delta\eta}\delta_{\alpha\beta}
\end{align}

#### Total

\begin{align}
 \nabla_\alpha V =& V_\alpha = -3\frac{(\mu r)}{r^5}r_\alpha + \frac{\mu_\alpha}{r^3} - 5\frac{(\theta rr)}{r^7}r_\alpha + 2\frac{(\theta r)_\alpha}{r^5} - 7\frac{(\Omega rrr)}{r^9}r_\alpha + 3\frac{(\Omega rr)_\alpha}{r^7} - 9\frac{(\Phi rrrr)}{r^{11}}r_\alpha + 4\frac{(\Phi rrr)_\alpha}{r^9} \\
 \nabla_\beta V_\alpha =& V_{\alpha\beta} = 15\frac{(\mu r)}{r^7}r_\alpha r_\beta - 3\frac{(\mu_\alpha r_\beta + \mu_\beta r_\alpha)}{r^5} + 35\frac{(\theta rr)}{r^9}r_\alpha r_\beta - 10\frac{((\theta r)_\alpha r_\beta + (\theta r)_\beta r_\alpha)}{r^7} + 2\frac{\theta_{\alpha\beta}}{r^5} \nonumber \\
 &+64\frac{(\Omega rrr)}{r^{11}}-21\frac{((\Omega rr)_\alpha r_\beta + (\Omega rr)_\beta r_\alpha)}{r^9} + 6\frac{(\Omega r)_{\alpha\beta}}{r^7}
 + 99\frac{(\Phi rrrr)}{r^{13}} - 36\frac{((\Phi rrr)_\alpha r_\beta + (\Phi rrr)_\beta r_\alpha)}{r^{11}} + 12\frac{(\Phi rr)_{\alpha\beta}}{r^9} \\
 V_{\alpha\alpha} =& V_{\alpha\beta}\delta_{\alpha\beta} - 3\frac{(\mu r)}{r^5} - 5\frac{(\theta rr)}{r^7} - 7\frac{(\Omega rrr)}{r^9} - 9\frac{(\Phi rrrr)}{r^{11}}
\end{align}




<h2>Model specifications</h2>

<h3> Rigid model :</h3>

<div class="main_container">
Each water molecule is treated as a rigid body with a fixed bond length and bond angle. We have chosen the experimentally determined molecular conformation to define the center of mass, but the interaction potential presented here is independent of that choice. A Cartesian coordinate system with the origin on the center of mass is defined as shown in Fig. 1. The center of mass was used as a
reference point in the calculation of the electrostatic and
induction components. The other components, i.e. the disper-
sion and repulsion, are naturally centered on the oxygen atom.
</div>





