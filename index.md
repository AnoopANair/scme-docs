## Introduction

A transferable and efficient polarizable interaction potential for extremely accurate solvent simulations in QM/MM systems.


![scme logo](images/scme_logo.jpg)

<!--more-->

**About SCME**
<div style="text-align: justify"> 

A potential function is presented for describing a system of flexible H<sub>2</sub>O molecules based on the single-center multipole expansion (SCME) of the electrostatic interaction. The model includes the variation of the molecular quadrupole moment as well as the dipole moment with changes in bond length and angle so as to reproduce results of high-level electronic structure calculations. The multipole expansion also includes fixed octupole and hexadecapole moments, as well as anisotropic dipole–dipole, dipole–quadrupole, and quadrupole–quadrupole polarizability tensors. The model contains five adjustable parameters related to the repulsive interaction and damping functions in the electrostatic and dispersion interactions. 
</div>

![scme ccsdt comparison](images/scme_ccsdt_comp.jpeg "Comparison of CCSD(T) energies of various cluster systems with that obtained via SCME")

<div style="text-align: justify"> 
In the case of 𝐻<sub>2</sub>𝑂 and 𝐶𝐻<sub>3</sub>𝐶𝑁, the entire electron distribution can be represented approximately by considering the lowest moments up to the hexadecapole. Moreover, including the effects of polarizability through induced moments helps to model the properties of the molecule in the presence of an external field. The single-center multipole expansion (SCME) method for describing molecular interactions developed by the EOJ group, evades the use of point charges (which requires computational demanding Ewald summation to converge) by performing multipole analysis on the molecule as a whole, rather than including a separate multipole expansion for each atom. The group has pioneered the method and developed transferable SCME potentials for modeling pure water(SCME-𝐻<sub>2</sub>𝑂) and pure acetonitrile (SCME-𝐶𝐻<sub>3</sub>𝐶𝑁).

</div>

For more information visit the [SCME](https://anoopanair.gitlab.io/scme-docs/) documentation.